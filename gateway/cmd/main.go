package main

import (
	"log"

	"gateaway/config"
	"gateaway/internal/app"
)

func main() {
	cfg, err := config.LoadFromJson("./config.json")
	if err != nil {
		log.Fatalf("Error then loading config: %s", err)
	}
	app.Run(cfg)
}
