package main

import (
	"database/sql"
	"fmt"
	"log"
)

// Up is executed when this migration is applied
func Up_20221027130253(txn *sql.Tx) {
	_, err := txn.Exec(`
CREATE TABLE balances
(
    user_id 	INT NOT NULL PRIMARY KEY,
    balance		INT NOT NULL
)
`)

	if err != nil {
		log.Fatalf("Up_20221027125135: %s\n", err)
		return
	}

	fmt.Println("Up_20221027125135 OK")
	return
}

// Down is executed when this migration is rolled back
func Down_20221027130253(txn *sql.Tx) {
	_, err := txn.Exec(`
DROP TABLE balances
`)

	if err != nil {
		log.Fatalf("Down_20221027125135: %s\n", err)
		return
	}

	fmt.Println("Down_20221027125135 OK")
	return
}
