package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
)

// Up is executed when this migration is applied
func Up_20221027130304(txn *sql.Tx) {
	uuids := [3]string{}
	for i := 0; i < 3; i++ {
		id, err := uuid.NewUUID()
		if err != nil {
			log.Fatalf("Up_20221027130304 #1: %s\n", err)
			return
		}
		uuids[i] = id.String()
	}

	_, err := txn.Exec(`
	INSERT INTO transactions (user_id, created_at, amount, uuid, status)
	VALUES
		   (1001,$1, 10, $2, 1),
		   (1002,$3, 100, $4, 1),
		   (1003,$5, 25, $6, 1)
	`,
		time.Now().Unix(), uuids[0], time.Now().Unix(), uuids[1], time.Now().Unix(), uuids[2])
	if err != nil {
		log.Fatalf("Up_20221027130304: %s\n", err)
		return
	}

	_, err = txn.Exec(`
	INSERT INTO balances (user_id, balance)
	VALUES
		   (1001,995),
		   (1002,3309),
		   (1003,10)
	`)
	if err != nil {
		log.Fatalf("Up_20221027130304: %s\n", err)
		return
	}

	fmt.Println("Up_20221027130304 OK")
	return
}

// Down is executed when this migration is rolled back
func Down_20221027130304(txn *sql.Tx) {

}
