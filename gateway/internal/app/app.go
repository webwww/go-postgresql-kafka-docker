package app

import (
	"fmt"
	"log"
	"net/http"

	"github.com/segmentio/kafka-go"

	"gateaway/config"
	"gateaway/internal/controller"
	"gateaway/internal/usecase"
	"gateaway/internal/usecase/queueReader"
	queueWriter "gateaway/internal/usecase/queueWriter"
	"gateaway/internal/usecase/repo"
	"gateaway/pkg/postgres"
)

func Run(config *config.Config) {
	pg, err := postgres.New(fmt.Sprintf("postgres://%s:%s@%s:%d/%s",
		config.Postgres.User,
		config.Postgres.Pass,
		config.Postgres.Host,
		config.Postgres.Port,
		config.Postgres.Database,
	))
	if err != nil {
		log.Println("Stop server, postgres err:", err)
		return
	}

	w := &kafka.Writer{
		Addr:                   kafka.TCP(config.Kafka.Nodes...),
		Balancer:               &kafka.Hash{},
		Topic:                  config.Kafka.WriterTopic,
		AllowAutoTopicCreation: true,
		Async:                  true,
	}

	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  config.Kafka.Nodes,
		Topic:    config.Kafka.ReaderTopic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	transactionUseCase := usecase.New(
		repo.New(pg),
		queueWriter.New(w),
		queueReader.New(reader),
	)

	s := http.NewServeMux()
	controller.NewRouter(s, *transactionUseCase).InitRoutes()
	fmt.Println("gateway locked and loaded")

	err = http.ListenAndServe(fmt.Sprintf(":%d", config.Port), s)
	if err != nil {
		log.Println("Stop server:", err)
		return
	}
}
