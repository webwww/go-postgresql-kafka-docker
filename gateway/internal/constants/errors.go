package constants

import "errors"

var (
	ErrCantWithdrawLessThenOne = errors.New("can't withdraw less than 1")
	ErrBalanceCantBeLessZero   = errors.New("balance can't be less then 0")
)
