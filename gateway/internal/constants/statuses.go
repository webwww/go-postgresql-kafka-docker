package constants

const (
	transactionSent      = 1
	transactionCancelled = 2
)

type transactionStatuses struct {
	Sent      int8
	Cancelled int8
}

var TransactionStatuses = transactionStatuses{
	Sent:      transactionSent,
	Cancelled: transactionCancelled,
}
