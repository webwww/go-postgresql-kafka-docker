package controller

import (
	"gateaway/internal/usecase"
)

func queueController(uc usecase.TransactionUseCase) {
	uc.ReadErrors()
}
