package controller

import (
	"net/http"

	"gateaway/internal/usecase"
)

type Handler struct {
	uc      usecase.TransactionUseCase
	handler *http.ServeMux
}

func NewRouter(handler *http.ServeMux, uc usecase.TransactionUseCase) *Handler {
	return &Handler{uc: uc, handler: handler}
}

func (h *Handler) InitRoutes() {
	newTransactionRoutes(h.handler, h.uc)
	queueController(h.uc)
}
