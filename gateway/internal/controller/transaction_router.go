package controller

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gateaway/internal/constants"
	"gateaway/internal/usecase"
)

type transactionRoutes struct {
	uc usecase.TransactionUseCase
}

func newTransactionRoutes(handler *http.ServeMux, uc usecase.TransactionUseCase) {
	routes := &transactionRoutes{uc}

	handler.HandleFunc("/setBalance", func(writer http.ResponseWriter, request *http.Request) {
		userIDStr := request.URL.Query().Get("userid")
		userID, err := strconv.Atoi(userIDStr)
		if err != nil {
			http.Error(writer, "incorrect userid", http.StatusBadRequest)
			return
		}

		amountStr := request.URL.Query().Get("amount")
		amount, err := strconv.Atoi(amountStr)
		if err != nil {
			http.Error(writer, "incorrect amount", http.StatusBadRequest)
			return
		}

		err = routes.uc.Proceed(request.Context(), int64(userID), int64(amount))
		switch errors.Unwrap(err) {
		case nil:
		case constants.ErrBalanceCantBeLessZero, constants.ErrCantWithdrawLessThenOne:
			http.Error(writer, err.Error(), http.StatusBadRequest)
			return
		default:
			fmt.Printf("newTransactionRoutes.setBalance #1, Err:%s\n", err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

	})
}
