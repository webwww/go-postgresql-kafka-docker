package entities

type Transaction struct {
	ID        int64  `json:"id" db:"id"`
	UserID    int64  `json:"userID" db:"user_id"`
	CreatedAt int64  `json:"createdAt" db:"created_at"`
	Amount    int64  `json:"amount" db:"amount"`
	UUID      string `json:"UUID" db:"uuid"`
	Status    int8   `json:"status" db:"status"`
}
