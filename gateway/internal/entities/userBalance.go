package entities

type UserBalance struct {
	UserID  int64 `json:"user_id" db:"user_id"`
	Balance int64 `json:"balance" db:"balance"`
}
