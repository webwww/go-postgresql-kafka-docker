package usecase

import (
	"context"

	"github.com/segmentio/kafka-go"

	"gateaway/internal/entities"
)

//go:generate mockgen -source=interfaces.go -destination=./mocks_test.go -package=usecase_test

type (
	TransactionRepo interface {
		GetBalance(context.Context, int64) (*entities.UserBalance, error)
		SaveTransaction(context.Context, *entities.Transaction) (string, error)
		InsertTransaction(context.Context, *entities.Transaction) error
		InsertBalance(context.Context, *entities.Transaction) error
		CompensateTransaction(context.Context, *entities.Transaction) error
	}

	TransactionQueueWriter interface {
		Write(context.Context, *entities.Transaction, string) error
	}

	TransactionQueueReader interface {
		ReadKafkaMessage() func(ctx context.Context) (kafka.Message, error)
	}
)
