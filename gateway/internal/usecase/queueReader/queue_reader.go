package queueReader

import (
	"context"

	"github.com/segmentio/kafka-go"
)

type TransactionQueueReader struct {
	*kafka.Reader
}

func New(reader *kafka.Reader) *TransactionQueueReader {
	return &TransactionQueueReader{reader}
}

func (r *TransactionQueueReader) ReadKafkaMessage() func(ctx context.Context) (kafka.Message, error) {
	return r.ReadMessage
}
