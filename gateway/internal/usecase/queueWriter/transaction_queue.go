package queueWriter

import (
	"context"
	"fmt"

	"github.com/segmentio/kafka-go"

	"gateaway/internal/entities"
)

type TransactionQueue struct {
	*kafka.Writer
}

func New(writer *kafka.Writer) *TransactionQueue {
	return &TransactionQueue{writer}
}

func (r *TransactionQueue) Write(ctx context.Context, transaction *entities.Transaction, uuid string) error {
	err := r.Writer.WriteMessages(
		ctx,
		kafka.Message{
			Key:   []byte(fmt.Sprintf("%d", transaction.UserID)),
			Value: []byte(fmt.Sprintf("%d|%s", transaction.Amount, uuid)),
		})
	if err != nil {
		return err
	}

	return nil
}
