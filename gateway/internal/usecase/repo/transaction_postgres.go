package repo

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"

	"gateaway/internal/constants"
	"gateaway/internal/entities"
	"gateaway/pkg/postgres"
)

type TransactionRepo struct {
	*postgres.Postgres
}

func New(pg *postgres.Postgres) *TransactionRepo {
	return &TransactionRepo{pg}
}

func (r *TransactionRepo) GetBalance(ctx context.Context, userID int64) (*entities.UserBalance, error) {
	var balance entities.UserBalance
	row, err := r.Pool.Query(ctx, "SELECT user_id, balance from balances where user_id=$1", userID)
	if err != nil {
		return nil, fmt.Errorf("TransactionRepo.GetBalance #1:, err: %s\n", err.Error())
	}
	err = pgxscan.ScanOne(&balance, row)
	switch errors.Unwrap(err) {
	case nil:
	case pgx.ErrNoRows:
		return &entities.UserBalance{UserID: userID, Balance: 0}, nil
	default:
		return nil, fmt.Errorf("TransactionRepo.GetBalance #2: , err: %s\n", err.Error())
	}

	return &balance, nil
}

func (r *TransactionRepo) SaveTransaction(ctx context.Context, transaction *entities.Transaction) (string, error) {
	var err error
	r.Tx, err = r.Pool.Begin(ctx)
	if err != nil {
		return "", fmt.Errorf("TransactionRepo.SaveTransaction #1, Err: %s", err.Error())
	}

	defer func() {
		if err == nil {
			return
		}
		txErr := r.Tx.Rollback(ctx)
		if err != nil {
			fmt.Printf("TransactionRepo.SaveTransaction #2, Err: %s", txErr)
			return
		}
	}()

	transactionUUID, err := uuid.NewUUID()
	if err != nil {
		return "", fmt.Errorf("TransactionRepo.SaveTransaction #3, Err: %s", err.Error())
	}
	transaction.UUID = transactionUUID.String()

	err = r.InsertTransaction(ctx, transaction)
	if err != nil {
		return "", fmt.Errorf("TransactionRepo.SaveTransaction #4, Err: %s", err.Error())
	}

	err = r.InsertBalance(ctx, transaction)
	if err != nil {
		return "", fmt.Errorf("TransactionRepo.SaveTransaction #5, Err: %s", err.Error())
	}

	err = r.Tx.Commit(ctx)
	if err != nil {
		return "", fmt.Errorf("TransactionRepo.SaveTransaction #6, Err: %s", err.Error())
	}

	return transaction.UUID, nil
}

func (r *TransactionRepo) InsertTransaction(ctx context.Context, transaction *entities.Transaction) error {
	_, err := r.Tx.Exec(ctx, "INSERT INTO transactions(user_id, created_at, amount, status, uuid) VALUES($1,$2,$3,$4,$5)",
		transaction.UserID, time.Now().Unix(), transaction.Amount, transaction.Status, transaction.UUID)
	return err
}

func (r *TransactionRepo) InsertBalance(ctx context.Context, transaction *entities.Transaction) error {
	_, err := r.Tx.Exec(ctx, "UPDATE balances SET balance = balance - $1 WHERE user_id = $2", transaction.Amount, transaction.UserID)
	return err
}

func (r *TransactionRepo) CompensateTransaction(ctx context.Context, transaction *entities.Transaction) error {
	var err error
	r.Tx, err = r.Pool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("TransactionRepo.CompensateTransaction #1, Err: %s", err.Error())
	}

	defer func() {
		if err == nil {
			return
		}
		txErr := r.Tx.Rollback(ctx)
		if err != nil {
			fmt.Printf("TransactionRepo.CompensateTransaction #2, Err: %s", txErr)
			return
		}
	}()

	currentStatus, err := r.SelectStatus(ctx, transaction)
	if err != nil {
		return fmt.Errorf("TransactionRepo.CompensateTransaction #3, Err: %s", err.Error())
	}
	if currentStatus == constants.TransactionStatuses.Cancelled {
		return nil
	}

	err = r.UpdateTransactionStatus(ctx, transaction)
	if err != nil {
		return fmt.Errorf("TransactionRepo.CompensateTransaction #4, Err: %s", err.Error())
	}

	err = r.IncreaseBalance(ctx, transaction)
	if err != nil {
		return fmt.Errorf("TransactionRepo.CompensateTransaction #5, Err: %s", err.Error())
	}

	err = r.Tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("TransactionRepo.CompensateTransaction #6, Err: %s", err.Error())
	}

	return nil
}

func (r *TransactionRepo) UpdateTransactionStatus(ctx context.Context, transaction *entities.Transaction) error {
	return r.Tx.QueryRow(ctx,
		"UPDATE transactions SET status = $1 WHERE uuid = $2 RETURNING user_id, amount",
		transaction.Status, transaction.UUID).Scan(&transaction.UserID, &transaction.Amount)
}

func (r *TransactionRepo) IncreaseBalance(ctx context.Context, transaction *entities.Transaction) error {
	_, err := r.Tx.Exec(ctx, "UPDATE balances SET balance = balance + $1 WHERE user_id = $2", transaction.Amount, transaction.UserID)
	return err
}

func (r *TransactionRepo) SelectStatus(ctx context.Context, transaction *entities.Transaction) (int8, error) {
	var currentStatus int8
	return currentStatus, r.Tx.QueryRow(ctx, "SELECT status FROM transactions WHERE uuid = $1 FOR UPDATE",
		transaction.UUID).Scan(&currentStatus)
}
