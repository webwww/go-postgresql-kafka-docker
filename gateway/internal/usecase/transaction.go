package usecase

import (
	"context"
	"fmt"

	"gateaway/internal/constants"
	"gateaway/internal/entities"
)

type TransactionUseCase struct {
	repo        TransactionRepo
	queueWriter TransactionQueueWriter
	queueReader TransactionQueueReader
}

func New(r TransactionRepo, qw TransactionQueueWriter, qr TransactionQueueReader) *TransactionUseCase {
	return &TransactionUseCase{
		repo:        r,
		queueWriter: qw,
		queueReader: qr,
	}
}

func (uc *TransactionUseCase) Proceed(ctx context.Context, userID int64, amount int64) error {
	userBalance, err := uc.repo.GetBalance(ctx, userID)
	if err != nil {
		return fmt.Errorf("TransactionUseCase.Proceed #1, Err:%s\n", err.Error())
	}
	if amount < 1 {
		return fmt.Errorf("%w", constants.ErrCantWithdrawLessThenOne)
	}

	if userBalance.Balance-amount < 0 {
		return fmt.Errorf("%w", constants.ErrBalanceCantBeLessZero)
	}

	transaction := &entities.Transaction{Amount: amount, UserID: userID, Status: constants.TransactionStatuses.Sent}
	uuid, err := uc.repo.SaveTransaction(ctx, transaction)
	if err != nil {
		return fmt.Errorf("TransactionUseCase.Proceed #2, Err:%s\n", err.Error())
	}

	err = uc.queueWriter.Write(ctx, transaction, uuid)
	if err != nil {
		return fmt.Errorf("TransactionUseCase.Proceed #3, Err:%s\n", err.Error())
	}

	return nil
}

func (uc *TransactionUseCase) ReadErrors() {
	go func() {
		for {
			ctx := context.Background()
			m, err := uc.queueReader.ReadKafkaMessage()(ctx)
			if err != nil {
				fmt.Printf("queueController: %s Error #1: \n", err.Error())
				return
			}
			fmt.Println("failed message receive:", string(m.Key))
			err = uc.CancelTransaction(ctx, string(m.Key))
			if err != nil {
				fmt.Printf("queueController: %s Error #2: \n", err.Error())
			}
		}
	}()
}

func (uc *TransactionUseCase) CancelTransaction(ctx context.Context, UUID string) error {
	transaction := entities.Transaction{UUID: UUID, Status: constants.TransactionStatuses.Cancelled}
	err := uc.repo.CompensateTransaction(ctx, &transaction)
	if err != nil {
		return fmt.Errorf("TransactionUseCase.CancelTransaction #1, Err:%s\n\n", err.Error())
	}

	return nil
}
