package usecase_test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"

	"gateaway/internal/constants"
	"gateaway/internal/entities"
	"gateaway/internal/usecase"
)

func transaction(t *testing.T) (*usecase.TransactionUseCase, *MockTransactionRepo, *MockTransactionQueueWriter) {
	t.Helper()

	mockCtl := gomock.NewController(t)
	defer mockCtl.Finish()

	repo := NewMockTransactionRepo(mockCtl)
	queueWrite := NewMockTransactionQueueWriter(mockCtl)
	queueRead := NewMockTransactionQueueReader(mockCtl)

	translation := usecase.New(repo, queueWrite, queueRead)

	return translation, repo, queueWrite
}

func Test_Proceed(t *testing.T) {
	t.Parallel()
	taction, repo, queue := transaction(t)

	tests := []struct {
		name         string
		mockBehavior func(context.Context, int64, int64)
		amount       int64
		isErr        bool
	}{
		{
			name: "OK",
			mockBehavior: func(ctx context.Context, userID int64, amount int64) {
				repo.EXPECT().GetBalance(ctx, userID).Return(&entities.UserBalance{UserID: userID, Balance: 1000}, nil)
				taction := &entities.Transaction{UserID: userID, Amount: amount, Status: constants.TransactionStatuses.Sent}
				repo.EXPECT().SaveTransaction(ctx, taction).Return("ANY UUID", nil)
				queue.EXPECT().Write(ctx, taction, "ANY UUID").Return(nil)
			},
			amount: 1000,
		},
		{
			name: "ErrCantWithdrawLessThenOne Err",
			mockBehavior: func(ctx context.Context, userID int64, amount int64) {
				repo.EXPECT().GetBalance(ctx, userID).Return(&entities.UserBalance{UserID: userID, Balance: 1000}, nil)
				taction := &entities.Transaction{UserID: userID, Amount: amount, Status: constants.TransactionStatuses.Sent}
				repo.EXPECT().SaveTransaction(ctx, taction).Return("ANY UUID", nil)
				queue.EXPECT().Write(ctx, taction, "ANY UUID").Return(nil)
			},
			amount: -1,
			isErr:  true,
		},
		{
			name: "ErrBalanceCantBeLessZero Err",
			mockBehavior: func(ctx context.Context, userID int64, amount int64) {
				repo.EXPECT().GetBalance(ctx, userID).Return(&entities.UserBalance{UserID: userID, Balance: 1}, nil)
				taction := &entities.Transaction{UserID: userID, Amount: amount, Status: constants.TransactionStatuses.Sent}
				repo.EXPECT().SaveTransaction(ctx, taction).Return("ANY UUID", nil)
				queue.EXPECT().Write(ctx, taction, "ANY UUID").Return(nil)
			},
			amount: 100,
			isErr:  true,
		},
		{
			name: "Zero balance OK",
			mockBehavior: func(ctx context.Context, userID int64, amount int64) {
				repo.EXPECT().GetBalance(ctx, userID).Return(&entities.UserBalance{UserID: userID, Balance: 1}, nil)
				taction := &entities.Transaction{UserID: userID, Amount: amount, Status: constants.TransactionStatuses.Sent}
				repo.EXPECT().SaveTransaction(ctx, taction).Return("ANY UUID", nil)
				queue.EXPECT().Write(ctx, taction, "ANY UUID").Return(nil)
			},
			amount: 1,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			var userID int64 = 1001
			test.mockBehavior(context.Background(), userID, test.amount)
			err := taction.Proceed(context.Background(), userID, test.amount)
			if err != nil && !test.isErr {
				t.Errorf("transactionUseCase.Procced err got: %s", err)
			}
			if test.isErr && err == nil {
				t.Errorf("transactionUseCase.Procced err want error, got: %s", err)
			}
		})
	}
}
