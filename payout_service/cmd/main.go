package main

import (
	"log"

	"payout_service/config"
	"payout_service/internal/app"
)

func main() {
	cfg, err := config.LoadFromJson("./config.json")
	if err != nil {
		log.Fatalf("Error then loading config: %s", err)
	}
	app.Run(cfg)
}
