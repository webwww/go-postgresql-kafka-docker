package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Config struct {
	Postgres Postgres `json:"postgres"`
	Kafka    Kafka    `json:"kafka"`
	Port     int      `json:"port"`
}

type Postgres struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Pass     string `json:"pass"`
	Database string `json:"database"`
}

type Kafka struct {
	Nodes       []string `json:"nodes"`
	WriterTopic string   `json:"writerTopic"`
	ReaderTopic string   `json:"readerTopic"`
}

func LoadFromJson(filePath string) (*Config, error) {
	var cfg Config
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("LoadFromJson #1, err: %w", err)
	}

	err = json.Unmarshal(file, &cfg)
	if err != nil {
		return nil, fmt.Errorf("LoadFromJson #2, err: %w", err)
	}

	return &cfg, nil
}
