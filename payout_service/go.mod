module payout_service

go 1.18

require (
	github.com/georgysavva/scany/v2 v2.0.0-alpha.3
	github.com/jackc/pgx/v4 v4.17.2
	github.com/jackc/pgx/v5 v5.0.4
	github.com/segmentio/kafka-go v0.4.35
	golang.org/x/net v0.1.0
)

require (
	bitbucket.org/liamstask/goose v0.0.0-20150115234039-8488cc47d90c // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.13.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.1 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.12.0 // indirect
	github.com/jackc/puddle/v2 v2.0.0 // indirect
	github.com/klauspost/compress v1.15.12 // indirect
	github.com/kylelemons/go-gypsy v1.0.0 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/ziutek/mymysql v1.5.4 // indirect
	golang.org/x/crypto v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)
