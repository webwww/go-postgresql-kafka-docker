package app

import (
	"fmt"

	"github.com/segmentio/kafka-go"

	"payout_service/config"
	"payout_service/internal/controller"
	"payout_service/internal/usecase"
	"payout_service/internal/usecase/repo"
	"payout_service/pkg/postgres"
)

func Run(cfg *config.Config) {
	pg, err := postgres.New(fmt.Sprintf("postgres://%s:%s@%s:%d/%s",
		cfg.Postgres.User,
		cfg.Postgres.Pass,
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.Database,
	))
	if err != nil {
		fmt.Printf("payout service.Run: Error #1:%s  \n\n", err.Error())
		return
	}

	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  cfg.Kafka.Nodes,
		Topic:    cfg.Kafka.ReaderTopic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	writer := &kafka.Writer{
		Addr:                   kafka.TCP(cfg.Kafka.Nodes...),
		Topic:                  cfg.Kafka.WriterTopic,
		Balancer:               &kafka.Hash{},
		AllowAutoTopicCreation: true,
		Async:                  true,
	}
	payoutUsecase := usecase.New(repo.New(pg))
	fmt.Println("loaded")
	controller.ListenQueue(reader, writer, payoutUsecase)
}
