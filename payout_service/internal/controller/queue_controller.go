package controller

import (
	"fmt"
	"runtime"
	"strconv"
	"strings"

	"github.com/segmentio/kafka-go"
	"golang.org/x/net/context"

	"payout_service/internal/entities"
	"payout_service/internal/usecase"
)

func ListenQueue(reader *kafka.Reader, writer *kafka.Writer, uc *usecase.PayoutUsecase) {
	var tokens = make(chan struct{}, runtime.NumCPU()) // max threads in semaphore
	for {
		m, err := reader.ReadMessage(context.Background())
		if err != nil {
			fmt.Printf("payout service.ListenQueue: Error #1: %s  \n\n", err.Error())
			return
		}
		userID, err := strconv.Atoi(string(m.Key))
		if err != nil {
			fmt.Printf("payout service.ListenQueue: Error #2: %s \n\n", err.Error())
			return
		}

		valuesSlice := strings.Split(string(m.Value), "|")
		amountStr := valuesSlice[0]
		uuid := valuesSlice[1]

		amount, err := strconv.Atoi(amountStr)
		if err != nil {
			fmt.Printf("payout service.ListenQueue: Error #3: %s \n\n", err.Error())
			return
		}
		receivedTransaction := &entities.Transaction{UserID: int64(userID), Amount: int64(amount), UUID: uuid}
		tokens <- struct{}{} // захват маркера
		go func(tx entities.Transaction) {
			err = uc.ProceedTransaction(&tx)
			if err == nil {
				return
			}
			fmt.Println("sending:", uuid)
			err = writer.WriteMessages(context.Background(), kafka.Message{Key: []byte(tx.UUID)})
			if err != nil {
				fmt.Printf("payout service.ListenQueue: Error #4:%s  \n\n", err.Error())
			}
			return
		}(*receivedTransaction)
		<-tokens // освобождение маркера
	}
}
