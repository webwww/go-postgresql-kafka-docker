package entities

type Transaction struct {
	UUID   string
	UserID int64
	Amount int64
}
