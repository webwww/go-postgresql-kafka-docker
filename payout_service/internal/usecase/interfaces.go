package usecase

import "payout_service/internal/entities"

type (
	PayoutRepo interface {
		DeduplicateTransaction(*entities.Transaction) error
	}
)
