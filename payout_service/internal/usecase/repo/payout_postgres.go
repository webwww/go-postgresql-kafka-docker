package repo

import (
	"context"
	"fmt"

	"payout_service/internal/entities"
	"payout_service/pkg/postgres"
)

type PayoutRepo struct {
	*postgres.Postgres
}

func New(pg *postgres.Postgres) *PayoutRepo {
	return &PayoutRepo{pg}
}

func (r *PayoutRepo) DeduplicateTransaction(transaction *entities.Transaction) error {
	_, err := r.Pool.Exec(context.Background(),
		"INSERT INTO incoming_transactions(uuid) VALUES($1) ON CONFLICT (uuid) DO NOTHING", transaction.UUID)
	if err != nil {
		return fmt.Errorf("payout service. proceedTransaction: %s Error #1: \n", err.Error())
	}
	return nil
}
