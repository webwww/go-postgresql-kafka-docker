package usecase

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	"payout_service/internal/entities"
)

type PayoutUsecase struct {
	repo PayoutRepo
}

func New(r PayoutRepo) *PayoutUsecase {
	return &PayoutUsecase{repo: r}
}

func (uc *PayoutUsecase) ProceedTransaction(receivedTransaction *entities.Transaction) error {
	err := uc.repo.DeduplicateTransaction(receivedTransaction)
	if err != nil {
		fmt.Printf("uuid: %s | success: %t \n", receivedTransaction.UUID, false)
		return fmt.Errorf("payout service.ProceedTransaction: Error #1: %w\n", err)
	}

	err = fakeLongTransactionWithError(receivedTransaction)
	if err != nil {
		fmt.Printf("uuid: %s | success: %t \n", receivedTransaction.UUID, false)
		return fmt.Errorf("payout service. proceedTransaction: Error #2: %w\n", err)
	}
	fmt.Printf("uuid: %s | success: %t \n", receivedTransaction.UUID, true)
	return nil
}

func fakeLongTransactionWithError(_ *entities.Transaction) error {
	// there can be some actions with argument
	// time.Sleep fakes latency

	rand.Seed(time.Now().UnixNano())
	if rand.Intn(2) == 1 {
		time.Sleep(time.Duration(rand.Intn(3)) * time.Second)
	} else {
		time.Sleep(time.Duration(rand.Intn(31)) * time.Second)
	}

	if rand.Intn(2) == 1 {
		return errors.New("mock error from payment service")
	}
	return nil
}
